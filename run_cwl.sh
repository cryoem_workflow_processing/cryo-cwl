#/bin/bash

# Set output folder path
processed=/mnt/data/exp/cryoem/TvNiR/processed/cwl_process

i=7
cwl-runner  --outdir $processed/out/$i/ \
            --tmpdir-prefix $processed/tmp/$i/ \
            --cachedir $processed/cache/$i/ \
            --parallel --timestamps 1>$processed/out.$i.log 2>&1 \
            cwl_workflows/process_full.cwl jobdata/process_full_job.yml