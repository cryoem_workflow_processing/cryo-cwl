#/bin/bash
source /mnt/data/sw/anaconda3/bin/activate

# Set output folder path
processed=/mnt/data/exp/cryoem/TvNiR/processed/cwl_process

i=1
cwl-runner  --outdir $processed/out/2_$i/ \
            --tmpdir-prefix $processed/tmp/2_$i/ \
            --cachedir $processed/cache/2_$i/ \
            --parallel --timestamps 1>$processed/out/2_$i.log 2>&1 \
            cwl_workflows/process_2.cwl jobdata/process_2_job.yml 