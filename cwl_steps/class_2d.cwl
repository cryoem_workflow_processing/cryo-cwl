#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Classify images by particle structure with EM algorithm.
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 1
requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_dir)
      - entryname: run.sh
        entry: >-
          mkdir $(inputs.outdir_name)

          relion_refine --i $(inputs.input_dir.basename)/$(inputs.extract_star_file) --o $(inputs.outdir_name)/run
          --pool $(inputs.pool) --iter $(inputs.iter) --K $(inputs.class_num) --tau2_fudge $(inputs.tau2_fudge)
          --particle_diameter $(inputs.particle_diameter) --strict_highres_exp $(inputs.strict_highres_exp)
          --oversampling $(inputs.oversampling) --offset_range $(inputs.offset_range) --offset_step $(inputs.offset_step)
          --psi_step $(inputs.psi_step) --ctf --flatten_solvent --zero_mask --norm --scale --j $(inputs.threads) --gpu
          --random_seed $(inputs.seed)

          ls $(inputs.outdir_name)/*| grep -v '$(inputs.iter)' | xargs rm --
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_dir:
    type: Directory
  extract_star_file:
    type: string
  outdir_name:
    type: string?
    default: Class2D
  pool:
    type: int?
    default: 100
  iter:
    type: int?
    default: 25
  tau2_fudge:
    type: int?
    default: 1
  particle_diameter:
    type: int?
    default: -1
  class_num:
    type: int?
    default: 1
  strict_highres_exp:
    type: int?
    default: -1
  oversampling:
    type: int?
    default: 1
  psi_step:
    type: int?
    default: -1
  offset_range:
    type: int?
    default: 6
  offset_step:
    type: int?
    default: 2
  threads:
    type: int?
    default: 3
  seed:
    type: int?
    default: 0
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
  star_file:
    type: string
    outputBinding:
      glob: '$(inputs.outdir_name)/run_*$(inputs.iter)_model.star'
      outputEval: $(self[0].basename)
