#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Extract picked particles
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 1
requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_micrographs_dir)
      - $(inputs.input_pick_dir)
      - entryname: run.sh
        entry: >-
          relion_preprocess --i $(inputs.input_micrographs_dir.basename)/$(inputs.micrographs_star_file)
          --coord_dir $(inputs.input_pick_dir.basename)/ --coord_suffix $(inputs.coord_suffix)
          --part_star $(inputs.outdir_name)/$(inputs.out_star_name) --part_dir $(inputs.outdir_name)/
          --extract --extract_size $(inputs.extract_size) --scale $(inputs.scale) --norm
          --bg_radius $(inputs.bg_radius) --white_dust $(inputs.white_dust) --black_dust $(inputs.black_dust)
          --invert_contrast --set_angpix $(inputs.angpix)
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_micrographs_dir:
    type: Directory
  micrographs_star_file:
    type: string
  input_pick_dir:
    type: Directory
  outdir_name:
    type: string
    default: Extract
  out_star_name:
    type: string
    default: particles.star
  coord_suffix:
    type: string
  angpix:
    type: float
  extract_size:
    type: int
  scale:
    type: int?
    default: -1
  bg_radius:
    type: int?
    default: -1
  white_dust:
    type: int?
    default: -1
  black_dust:
    type: int?
    default: -1
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
  star_file:
    type: string
    outputBinding:
      outputEval: $(inputs.out_star_name)
