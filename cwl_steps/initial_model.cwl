#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Create Initial 3D model by input images
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 1
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_extract_dir)
      - $(inputs.input_select_dir)
      - entryname: run.sh
        entry: >-
          mkdir $(inputs.outdir_name)

          relion_refine --i $(inputs.input_select_dir.basename)/$(inputs.select_star_file) --o $(inputs.outdir_name)/run 
          --sgd_ini_iter $(inputs.sgd_ini_iter) --sgd_inbetween_iter $(inputs.sgd_inbetween_iter) --sgd_fin_iter $(inputs.sgd_fin_iter)
          --sgd_write_iter 10 --sgd_ini_resol 35 --sgd_fin_resol 15 --sgd_ini_subset 100 --sgd_fin_subset 500 --sgd
          --denovo_3dref --ctf --ctf_intact_first_peak --flatten_solvent  --zero_mask  --dont_combine_weights_via_disc
          --K 1 --pool $(inputs.pool) --healpix_order $(inputs.healpix_order)  
          --particle_diameter $(inputs.particle_diameter) --sym $(inputs.symmetry)  
          --offset_range $(inputs.offset_range) --offset_step $(inputs.offset_step) --j $(inputs.threads)
          --random_seed $(inputs.seed)
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_extract_dir:
    type: Directory
  input_select_dir:
    type: Directory
  select_star_file:
    type: string
  outdir_name:
    type: string?
    default: InitialModel
  pool:
    type: int?
    default: 3
  sgd_ini_iter:
    type: int?
    default: 50
  sgd_inbetween_iter:
    type: int?
    default: 200
  sgd_fin_iter:
    type: int?
    default: 50
  healpix_order:
    type: int?
    default: 1
  particle_diameter:
    type: int?
    default: -1
  symmetry:
    type: string?
    default: c1
  offset_range:
    type: int?
    default: 6
  offset_step:
    type: int?
    default: 2
  threads:
    type: int?
    default: 1
  seed:
    type: int?
    default: 0
outputs:
  model_file:
    type: File
    outputBinding:
      glob: '$(inputs.outdir_name)/run_*$(inputs.sgd_ini_iter + inputs.sgd_inbetween_iter + inputs.sgd_fin_iter)_class001.mrc'
