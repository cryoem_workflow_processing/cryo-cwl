#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Combine array of Directory and included star files
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 1
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: >-
          mkdir $(inputs.outdir_name)

          declare -A star_files;
          declare -A mrc_files

          for f in \$(echo '$(inputs.input_dirs.map(function(dir) {return dir.path}).join(" "))'| tr " " "\\n")

          do
            star_files[$f]=\$(find $f/ -maxdepth 1 -name *.star) ;
            mrc_files[$f]=\$(cat \${star_files[$f]} | tr " " "\\n" | grep .mrc\$ -m 1) ;
            cp -rf $f/* $(inputs.outdir_name)/
          done

          sorted_files=\$(
          for k in "\${!star_files[@]}"

          do
            echo \${star_files[$k]} \${mrc_files[$k]}
          done |
          sort -k2 | cut -f1 -d" " | tr "\\n" " " )
          
          star_name=\$(find $(inputs.outdir_name)/ -maxdepth 1 -name *.star)

          echo "sorted files:" $sorted_files

          echo "star_name:" $star_name

          relion_star_combine --i "$sorted_files" --o $star_name
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_dirs:
    type: Directory[]
  outdir_name:
    type: string?
    default: Extract
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)

