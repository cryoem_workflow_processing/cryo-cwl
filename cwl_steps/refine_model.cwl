#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Refine 3D model
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 3
requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_dir)
      - entryname: run.sh
        entry: >-
          mkdir $(inputs.outdir_name)

          mpirun -np $(inputs.processes) relion_refine_mpi --i $(inputs.input_dir.basename)/$(inputs.star_file)
          --ref $(inputs.model_file.path) --o $(inputs.outdir_name)/run --particle_diameter 200 
          --pool $(inputs.pool) --healpix_order $(inputs.healpix_order)
          --particle_diameter $(inputs.particle_diameter) --sym $(inputs.symmetry)
          --low_resol_join_halves $(inputs.low_resol_join_halves) --ini_high $(inputs.ini_high)
          --auto_refine --split_random_halves --dont_combine_weights_via_disc 
          --ctf --ctf_corrected_ref --ctf_intact_first_peak --flatten_solvent --zero_mask --norm --scale  
          --offset_range $(inputs.offset_range) --offset_step $(inputs.offset_step) --j 1 --gpu
          --random_seed $(inputs.seed)

          rm -rf $(inputs.outdir_name)/*_it*
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_dir:
    type: Directory
  star_file:
    type: string
  model_file:
    type: File
  outdir_name:
    type: string?
    default: Refine3D
  pool:
    type: int?
    default: 100
  healpix_order:
    type: int?
    default: 1
  particle_diameter:
    type: int?
    default: -1
  symmetry:
    type: string?
    default: c1
  low_resol_join_halves:
    type: int?
    default: -1
  ini_high:
    type: int?
    default: -1
  offset_range:
    type: int?
    default: 6
  offset_step:
    type: int?
    default: 2
  processes:
    type: int?
    default: 3
  seed:
    type: int?
    default: 0
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
