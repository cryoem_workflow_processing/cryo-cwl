#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Autopick particles
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 1
requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_dir)
      - entryname: run.sh
        entry: >-
          relion_autopick --i $(inputs.input_dir.basename)/$(inputs.star_file) --odir $(inputs.outdir_name)/
          --angpix $(inputs.angpix) --pickname $(inputs.pickname) --LoG --LoG_diam_min $(inputs.LoG_diam_min)
          --LoG_diam_max $(inputs.LoG_diam_max) --LoG_adjust_threshold $(inputs.LoG_adjust_threshold)
          --shrink $(inputs.shrink) --lowpass $(inputs.lowpass) --random_seed $(inputs.seed)
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_dir:
    type: Directory
  star_file:
    type: string
  outdir_name:
    type: string?
    default: AutoPick
  pickname:
    type: string?
    default: autopick
  angpix:
    type: float
  LoG_diam_min:
    type: int
  LoG_diam_max:
    type: int
  shrink:
    type: float?
    default: 1
  lowpass:
    type: int?
    default: -1
  LoG_adjust_threshold:
    type: float?
    default: 0
  threads:
    type: int?
    default: 1
  seed:
    type: int?
    default: 0
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
  coord_suffix:
    type: string
    outputBinding:
      outputEval: "_$(inputs.pickname).star"
