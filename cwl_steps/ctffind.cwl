#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Estimate CTF with ctffind
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 1
requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_dir)
      - entryname: run.sh
        entry: >-
          relion_run_ctffind --i $(inputs.input_dir.basename)/$(inputs.star_file) --o $(inputs.outdir_name)/
          --CS $(inputs.sphere_aberration) --HT $(inputs.voltage) --AmpCnst $(inputs.amp_contrast)
          --XMAG $(inputs.magnification) --DStep $(inputs.angpix) --Box $(inputs.box) --ResMin $(inputs.res_min)
          --ResMax $(inputs.res_max) --dFMin $(inputs.df_min) --dFMax $(inputs.df_max) --FStep $(inputs.df_step)
          --dAst $(inputs.dAst) --ctfWin -1 --is_ctffind4

          sed -i "s/^$(inputs.input_dir.basename)/$(inputs.outdir_name)\\\/$(inputs.input_dir.basename)/g" $(inputs.outdir_name)/micrographs_ctf.star

          for f in \$(find $(inputs.outdir_name) -type l)

          do
            cp --remove-destination \$(readlink -e $f) $f
          done
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_dir:
    type: Directory
  star_file:
    type: string
  outdir_name:
    type: string?
    default: CtfFind
  sphere_aberration:
    type: float?
    default: 2.0
  angpix:
    type: float
  voltage:
    type: int?
    default: 300
  amp_contrast:
    type: float?
    default: 0.1
  magnification:
    type: int?
    default: 60000
  box:
    type: int?
    default: 512
  res_min:
    type: int?
    default: 100
  res_max:
    type: int?
    default: 7
  df_min:
    type: int?
    default: 10000
  df_max:
    type: int?
    default: 50000
  df_step:
    type: int?
    default: 250
  dAst:
    type: int?
    default: 0
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
