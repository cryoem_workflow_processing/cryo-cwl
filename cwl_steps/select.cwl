#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Select classified particles
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3_display
requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_extract_dir)
      - $(inputs.input_class_dir)
      - entryname: run.sh
        entry: >-
          mkdir $(inputs.outdir_name)
          
          xpra start --bind-tcp=0.0.0.0:10000 --daemon=no --exit-with-children 
          --start-child="relion_display --sort rlnClassDistribution --reverse
           --i $(inputs.input_class_dir.basename)/$(inputs.class_star_file)
           --display rlnReferenceImage --class --allow_save
           --fn_parts $(inputs.outdir_name)/$(inputs.out_star_name)
           --fn_imgs $(inputs.outdir_name)/$(inputs.out_class_avg_name) --recenter"

          if test -f "$(inputs.outdir_name)/$(inputs.out_star_name)"; then
            exit 0
          fi
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_extract_dir:
    type: Directory
  input_class_dir:
    type: Directory
  class_star_file:
    type: string
  outdir_name:
    type: string?
    default: Select
  out_star_name:
    type: string?
    default: particles.star
  out_class_avg_name:
    type: string?
    default: class_averages.star
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
  star_file:
    type: string
    outputBinding:
      outputEval: $(inputs.out_star_name)
