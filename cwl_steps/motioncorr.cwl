#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Correct input micrograph with MotionCorr
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 2
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: >-
          mkdir -p input ;
          cp "$(inputs.input_file.path)" input/ ;
          relion_star_loopheader rlnMicrographMovieName > movies.star ;
          ls input/* >> movies.star ;
          relion_run_motioncorr --use_motioncor2 --i movies.star --o $(inputs.outdir_name)/
          --first_frame_sum 1 --last_frame_sum 30 --bin_factor $(inputs.bin_factor) --bfactor $(inputs.bfactor)
          --angpix $(inputs.angpix) --voltage $(inputs.voltage) --dose_per_frame $(inputs.dose_per_frame)
          --preexposure $(inputs.preexposure) --patch_x $(inputs.patch_x) --patch_y $(inputs.patch_y)
          --dose_weighting --j 1 --max_io_threads 1 --use_own --save_noDW ;
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_file:
    type: File
  outdir_name:
    type: string
    default: MotionCorr
  bin_factor:
    type: int?
    default: 1
  bfactor:
    type: int?
    default: 150
  angpix:
    type: float
  voltage:
    type: int
  dose_per_frame:
    type: float
  preexposure:
    type: float?
    default: 0
  patch_x:
    type: int?
    default: 5
  patch_y:
    type: int?
    default: 5
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)

