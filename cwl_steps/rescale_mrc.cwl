#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Rescale mrc image or particles
hints:
  DockerRequirement:
    dockerPull: sbobkov/relion3
  ResourceRequirement:
    coresMin: 1
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: >-
          relion_image_handler --i $(inputs.input_mrc.path) --o $(inputs.outfile_name)
          --new_box $(inputs.new_box) --rescale_angpix $(inputs.angpix)
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_mrc:
    type: File
  outfile_name:
    type: string
  new_box:
    type: int
  angpix:
    type: float
outputs:
  model_file:
    type: File
    outputBinding:
      glob: '$(inputs.outfile_name)'
