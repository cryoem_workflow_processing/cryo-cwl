#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Select classified particles by cinderella
hints:
  DockerRequirement:
    dockerPull: sbobkov/cinderella
  ResourceRequirement:
    coresMin: 1
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - $(inputs.input_class_dir)
      - entryname: run.sh
        entry: >-
          sp_cinderella_predict.py -i $(inputs.input_class_dir.basename)/$(inputs.class_star_file.replace("_model.star", "_classes.mrcs"))
          -w /src/model.h5 -o cinderella_tmp_output -t $(inputs.threshold) &&
          /src/create_particles.py $(inputs.input_class_dir.basename)/$(inputs.class_star_file.replace("_model.star", "_data.star"))
          -o $(inputs.outdir_name)/$(inputs.out_star_name) -c cinderella_tmp_output/*index_confidence.txt -t $(inputs.threshold) &&
          cp cinderella_tmp_output/* $(inputs.outdir_name)
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_class_dir:
    type: Directory
  class_star_file:
    type: string
  threshold:
    type: float?
    default: 0.5
  outdir_name:
    type: string?
    default: Select
  out_star_name:
    type: string?
    default: particles.star
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
  star_file:
    type: string
    outputBinding:
      outputEval: $(inputs.out_star_name)
