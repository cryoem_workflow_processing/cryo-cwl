You can find documentation for Cryo CWL workflows in gitlab Wiki!

Documentation:
* [Installation](https://gitlab.com/cryoem_workflow_processing/cryo-cwl/-/wikis/Installation)
* [Running analysis](https://gitlab.com/cryoem_workflow_processing/cryo-cwl/-/wikis/Running%20analysis)
* [Description of CWL documents](https://gitlab.com/cryoem_workflow_processing/cryo-cwl/-/wikis/CWL%20Description/CWL%20Documents)