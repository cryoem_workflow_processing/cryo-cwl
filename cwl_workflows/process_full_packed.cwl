{
    "$graph": [
        {
            "class": "CommandLineTool",
            "label": "Autopick particles",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "relion_autopick --i $(inputs.input_dir.basename)/$(inputs.star_file) --odir $(inputs.outdir_name)/ --angpix $(inputs.angpix) --pickname $(inputs.pickname) --LoG --LoG_diam_min $(inputs.LoG_diam_min) --LoG_diam_max $(inputs.LoG_diam_max) --LoG_adjust_threshold $(inputs.LoG_adjust_threshold) --shrink $(inputs.shrink) --lowpass $(inputs.lowpass) --random_seed $(inputs.seed)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0,
                    "id": "#autopick.cwl/LoG_adjust_threshold"
                },
                {
                    "type": "int",
                    "id": "#autopick.cwl/LoG_diam_max"
                },
                {
                    "type": "int",
                    "id": "#autopick.cwl/LoG_diam_min"
                },
                {
                    "type": "float",
                    "id": "#autopick.cwl/angpix"
                },
                {
                    "type": "Directory",
                    "id": "#autopick.cwl/input_dir"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#autopick.cwl/lowpass"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "AutoPick",
                    "id": "#autopick.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "autopick",
                    "id": "#autopick.cwl/pickname"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#autopick.cwl/seed"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 1,
                    "id": "#autopick.cwl/shrink"
                },
                {
                    "type": "string",
                    "id": "#autopick.cwl/star_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#autopick.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "string",
                    "outputBinding": {
                        "outputEval": "_$(inputs.pickname).star"
                    },
                    "id": "#autopick.cwl/coord_suffix"
                },
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#autopick.cwl/outdir"
                }
            ],
            "id": "#autopick.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Classify images by particle structure with EM algorithm.",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "mkdir $(inputs.outdir_name)\nrelion_refine --i $(inputs.input_dir.basename)/$(inputs.extract_star_file) --o $(inputs.outdir_name)/run --pool $(inputs.pool) --iter $(inputs.iter) --K $(inputs.class_num) --tau2_fudge $(inputs.tau2_fudge) --particle_diameter $(inputs.particle_diameter) --strict_highres_exp $(inputs.strict_highres_exp) --oversampling $(inputs.oversampling) --offset_range $(inputs.offset_range) --offset_step $(inputs.offset_step) --psi_step $(inputs.psi_step) --ctf --flatten_solvent --zero_mask --norm --scale --j $(inputs.threads) --gpu --random_seed $(inputs.seed)\nls $(inputs.outdir_name)/*| grep -v '$(inputs.iter)' | xargs rm --"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#class_2d.cwl/class_num"
                },
                {
                    "type": "string",
                    "id": "#class_2d.cwl/extract_star_file"
                },
                {
                    "type": "Directory",
                    "id": "#class_2d.cwl/input_dir"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 25,
                    "id": "#class_2d.cwl/iter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 6,
                    "id": "#class_2d.cwl/offset_range"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 2,
                    "id": "#class_2d.cwl/offset_step"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "Class2D",
                    "id": "#class_2d.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#class_2d.cwl/oversampling"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#class_2d.cwl/particle_diameter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 100,
                    "id": "#class_2d.cwl/pool"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#class_2d.cwl/psi_step"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#class_2d.cwl/seed"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#class_2d.cwl/strict_highres_exp"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#class_2d.cwl/tau2_fudge"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 3,
                    "id": "#class_2d.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#class_2d.cwl/outdir"
                },
                {
                    "type": "string",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/run_*$(inputs.iter)_model.star",
                        "outputEval": "$(self[0].basename)"
                    },
                    "id": "#class_2d.cwl/star_file"
                }
            ],
            "id": "#class_2d.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Combine array of Directory and included star files",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "run.sh",
                            "entry": "mkdir $(inputs.outdir_name)\ndeclare -A star_files; declare -A mrc_files\nfor f in \\$(echo '$(inputs.input_dirs.map(function(dir) {return dir.path}).join(\" \"))'| tr \" \" \"\\\\n\")\ndo\n  star_files[$f]=\\$(find $f/ -maxdepth 1 -name *.star) ;\n  mrc_files[$f]=\\$(cat \\${star_files[$f]} | tr \" \" \"\\\\n\" | grep .mrc\\$ -m 1) ;\n  cp -rf $f/* $(inputs.outdir_name)/\ndone\nsorted_files=\\$( for k in \"\\${!star_files[@]}\"\ndo\n  echo \\${star_files[$k]} \\${mrc_files[$k]}\ndone | sort -k2 | cut -f1 -d\" \" | tr \"\\\\n\" \" \" )\nstar_name=\\$(find $(inputs.outdir_name)/ -maxdepth 1 -name *.star)\necho \"sorted files:\" $sorted_files\necho \"star_name:\" $star_name\nrelion_star_combine --i \"$sorted_files\" --o $star_name"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "Directory"
                    },
                    "id": "#combine_star_data.cwl/input_dirs"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "Extract",
                    "id": "#combine_star_data.cwl/outdir_name"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#combine_star_data.cwl/outdir"
                }
            ],
            "id": "#combine_star_data.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Estimate CTF with ctffind",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "relion_run_ctffind --i $(inputs.input_dir.basename)/$(inputs.star_file) --o $(inputs.outdir_name)/ --CS $(inputs.sphere_aberration) --HT $(inputs.voltage) --AmpCnst $(inputs.amp_contrast) --XMAG $(inputs.magnification) --DStep $(inputs.angpix) --Box $(inputs.box) --ResMin $(inputs.res_min) --ResMax $(inputs.res_max) --dFMin $(inputs.df_min) --dFMax $(inputs.df_max) --FStep $(inputs.df_step) --dAst $(inputs.dAst) --ctfWin -1 --is_ctffind4\nsed -i \"s/^$(inputs.input_dir.basename)/$(inputs.outdir_name)\\\\\\/$(inputs.input_dir.basename)/g\" $(inputs.outdir_name)/micrographs_ctf.star\nfor f in \\$(find $(inputs.outdir_name) -type l)\ndo\n  cp --remove-destination \\$(readlink -e $f) $f\ndone"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0.1,
                    "id": "#ctffind.cwl/amp_contrast"
                },
                {
                    "type": "float",
                    "id": "#ctffind.cwl/angpix"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 512,
                    "id": "#ctffind.cwl/box"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#ctffind.cwl/dAst"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 50000,
                    "id": "#ctffind.cwl/df_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 10000,
                    "id": "#ctffind.cwl/df_min"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 250,
                    "id": "#ctffind.cwl/df_step"
                },
                {
                    "type": "Directory",
                    "id": "#ctffind.cwl/input_dir"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 60000,
                    "id": "#ctffind.cwl/magnification"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "CtfFind",
                    "id": "#ctffind.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 7,
                    "id": "#ctffind.cwl/res_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 100,
                    "id": "#ctffind.cwl/res_min"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 2.0,
                    "id": "#ctffind.cwl/sphere_aberration"
                },
                {
                    "type": "string",
                    "id": "#ctffind.cwl/star_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 300,
                    "id": "#ctffind.cwl/voltage"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#ctffind.cwl/outdir"
                }
            ],
            "id": "#ctffind.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Extract picked particles",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_micrographs_dir)",
                        "$(inputs.input_pick_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "relion_preprocess --i $(inputs.input_micrographs_dir.basename)/$(inputs.micrographs_star_file) --coord_dir $(inputs.input_pick_dir.basename)/ --coord_suffix $(inputs.coord_suffix) --part_star $(inputs.outdir_name)/$(inputs.out_star_name) --part_dir $(inputs.outdir_name)/ --extract --extract_size $(inputs.extract_size) --scale $(inputs.scale) --norm --bg_radius $(inputs.bg_radius) --white_dust $(inputs.white_dust) --black_dust $(inputs.black_dust) --invert_contrast --set_angpix $(inputs.angpix)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": "float",
                    "id": "#extract_pick.cwl/angpix"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#extract_pick.cwl/bg_radius"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#extract_pick.cwl/black_dust"
                },
                {
                    "type": "string",
                    "id": "#extract_pick.cwl/coord_suffix"
                },
                {
                    "type": "int",
                    "id": "#extract_pick.cwl/extract_size"
                },
                {
                    "type": "Directory",
                    "id": "#extract_pick.cwl/input_micrographs_dir"
                },
                {
                    "type": "Directory",
                    "id": "#extract_pick.cwl/input_pick_dir"
                },
                {
                    "type": "string",
                    "id": "#extract_pick.cwl/micrographs_star_file"
                },
                {
                    "type": "string",
                    "default": "particles.star",
                    "id": "#extract_pick.cwl/out_star_name"
                },
                {
                    "type": "string",
                    "default": "Extract",
                    "id": "#extract_pick.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#extract_pick.cwl/scale"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#extract_pick.cwl/white_dust"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#extract_pick.cwl/outdir"
                },
                {
                    "type": "string",
                    "outputBinding": {
                        "outputEval": "$(inputs.out_star_name)"
                    },
                    "id": "#extract_pick.cwl/star_file"
                }
            ],
            "id": "#extract_pick.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Create Initial 3D model by input images",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_extract_dir)",
                        "$(inputs.input_select_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "mkdir $(inputs.outdir_name)\nrelion_refine --i $(inputs.input_select_dir.basename)/$(inputs.select_star_file) --o $(inputs.outdir_name)/run  --sgd_ini_iter $(inputs.sgd_ini_iter) --sgd_inbetween_iter $(inputs.sgd_inbetween_iter) --sgd_fin_iter $(inputs.sgd_fin_iter) --sgd_write_iter 10 --sgd_ini_resol 35 --sgd_fin_resol 15 --sgd_ini_subset 100 --sgd_fin_subset 500 --sgd --denovo_3dref --ctf --ctf_intact_first_peak --flatten_solvent  --zero_mask  --dont_combine_weights_via_disc --K 1 --pool $(inputs.pool) --healpix_order $(inputs.healpix_order)   --particle_diameter $(inputs.particle_diameter) --sym $(inputs.symmetry)   --offset_range $(inputs.offset_range) --offset_step $(inputs.offset_step) --j $(inputs.threads) --random_seed $(inputs.seed)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#initial_model.cwl/healpix_order"
                },
                {
                    "type": "Directory",
                    "id": "#initial_model.cwl/input_extract_dir"
                },
                {
                    "type": "Directory",
                    "id": "#initial_model.cwl/input_select_dir"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 6,
                    "id": "#initial_model.cwl/offset_range"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 2,
                    "id": "#initial_model.cwl/offset_step"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "InitialModel",
                    "id": "#initial_model.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#initial_model.cwl/particle_diameter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 3,
                    "id": "#initial_model.cwl/pool"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#initial_model.cwl/seed"
                },
                {
                    "type": "string",
                    "id": "#initial_model.cwl/select_star_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 50,
                    "id": "#initial_model.cwl/sgd_fin_iter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 200,
                    "id": "#initial_model.cwl/sgd_inbetween_iter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 50,
                    "id": "#initial_model.cwl/sgd_ini_iter"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "c1",
                    "id": "#initial_model.cwl/symmetry"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#initial_model.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/run_*$(inputs.sgd_ini_iter + inputs.sgd_inbetween_iter + inputs.sgd_fin_iter)_class001.mrc"
                    },
                    "id": "#initial_model.cwl/model_file"
                }
            ],
            "id": "#initial_model.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Correct input micrograph with MotionCorr",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 2,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "run.sh",
                            "entry": "mkdir -p input ; cp \"$(inputs.input_file.path)\" input/ ; relion_star_loopheader rlnMicrographMovieName > movies.star ; ls input/* >> movies.star ; relion_run_motioncorr --use_motioncor2 --i movies.star --o $(inputs.outdir_name)/ --first_frame_sum 1 --last_frame_sum 30 --bin_factor $(inputs.bin_factor) --bfactor $(inputs.bfactor) --angpix $(inputs.angpix) --voltage $(inputs.voltage) --dose_per_frame $(inputs.dose_per_frame) --preexposure $(inputs.preexposure) --patch_x $(inputs.patch_x) --patch_y $(inputs.patch_y) --dose_weighting --j 1 --max_io_threads 1 --use_own --save_noDW ;"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": "float",
                    "id": "#motioncorr.cwl/angpix"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 150,
                    "id": "#motioncorr.cwl/bfactor"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#motioncorr.cwl/bin_factor"
                },
                {
                    "type": "float",
                    "id": "#motioncorr.cwl/dose_per_frame"
                },
                {
                    "type": "File",
                    "id": "#motioncorr.cwl/input_file"
                },
                {
                    "type": "string",
                    "default": "MotionCorr",
                    "id": "#motioncorr.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 5,
                    "id": "#motioncorr.cwl/patch_x"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 5,
                    "id": "#motioncorr.cwl/patch_y"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0,
                    "id": "#motioncorr.cwl/preexposure"
                },
                {
                    "type": "int",
                    "id": "#motioncorr.cwl/voltage"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#motioncorr.cwl/outdir"
                }
            ],
            "id": "#motioncorr.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Reextract particles",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_micrographs_dir)",
                        "$(inputs.input_select_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "relion_preprocess --i $(inputs.input_micrographs_dir.basename)/$(inputs.micrographs_star_file) --reextract_data_star $(inputs.input_select_dir.basename)/$(inputs.select_star_file) --part_star $(inputs.outdir_name)/$(inputs.out_star_name) --part_dir $(inputs.outdir_name)/ --extract --extract_size $(inputs.extract_size) --scale $(inputs.scale) --norm --bg_radius $(inputs.bg_radius) --white_dust $(inputs.white_dust) --black_dust $(inputs.black_dust) --invert_contrast --set_angpix $(inputs.angpix)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": "float",
                    "id": "#reextract.cwl/angpix"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#reextract.cwl/bg_radius"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#reextract.cwl/black_dust"
                },
                {
                    "type": "int",
                    "id": "#reextract.cwl/extract_size"
                },
                {
                    "type": "Directory",
                    "id": "#reextract.cwl/input_micrographs_dir"
                },
                {
                    "type": "Directory",
                    "id": "#reextract.cwl/input_select_dir"
                },
                {
                    "type": "string",
                    "id": "#reextract.cwl/micrographs_star_file"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "particles.star",
                    "id": "#reextract.cwl/out_star_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "Reextract",
                    "id": "#reextract.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#reextract.cwl/scale"
                },
                {
                    "type": "string",
                    "id": "#reextract.cwl/select_star_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#reextract.cwl/white_dust"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#reextract.cwl/outdir"
                },
                {
                    "type": "string",
                    "outputBinding": {
                        "outputEval": "$(inputs.out_star_name)"
                    },
                    "id": "#reextract.cwl/star_file"
                }
            ],
            "id": "#reextract.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Refine 3D model",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 3,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "mkdir $(inputs.outdir_name)\nmpirun -np $(inputs.processes) relion_refine_mpi --i $(inputs.input_dir.basename)/$(inputs.star_file) --ref $(inputs.model_file.path) --o $(inputs.outdir_name)/run --particle_diameter 200  --pool $(inputs.pool) --healpix_order $(inputs.healpix_order) --particle_diameter $(inputs.particle_diameter) --sym $(inputs.symmetry) --low_resol_join_halves $(inputs.low_resol_join_halves) --ini_high $(inputs.ini_high) --auto_refine --split_random_halves --dont_combine_weights_via_disc  --ctf --ctf_corrected_ref --ctf_intact_first_peak --flatten_solvent --zero_mask --norm --scale   --offset_range $(inputs.offset_range) --offset_step $(inputs.offset_step) --j 1 --gpu --random_seed $(inputs.seed)\nrm -rf $(inputs.outdir_name)/*_it*"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#refine_model.cwl/healpix_order"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#refine_model.cwl/ini_high"
                },
                {
                    "type": "Directory",
                    "id": "#refine_model.cwl/input_dir"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#refine_model.cwl/low_resol_join_halves"
                },
                {
                    "type": "File",
                    "id": "#refine_model.cwl/model_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 6,
                    "id": "#refine_model.cwl/offset_range"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 2,
                    "id": "#refine_model.cwl/offset_step"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "Refine3D",
                    "id": "#refine_model.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": -1,
                    "id": "#refine_model.cwl/particle_diameter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 100,
                    "id": "#refine_model.cwl/pool"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 3,
                    "id": "#refine_model.cwl/processes"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#refine_model.cwl/seed"
                },
                {
                    "type": "string",
                    "id": "#refine_model.cwl/star_file"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "c1",
                    "id": "#refine_model.cwl/symmetry"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#refine_model.cwl/outdir"
                }
            ],
            "id": "#refine_model.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Rescale mrc image or particles",
            "hints": [
                {
                    "dockerPull": "sbobkov/relion3",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "run.sh",
                            "entry": "relion_image_handler --i $(inputs.input_mrc.path) --o $(inputs.outfile_name) --new_box $(inputs.new_box) --rescale_angpix $(inputs.angpix)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": "float",
                    "id": "#rescale_mrc.cwl/angpix"
                },
                {
                    "type": "File",
                    "id": "#rescale_mrc.cwl/input_mrc"
                },
                {
                    "type": "int",
                    "id": "#rescale_mrc.cwl/new_box"
                },
                {
                    "type": "string",
                    "id": "#rescale_mrc.cwl/outfile_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outfile_name)"
                    },
                    "id": "#rescale_mrc.cwl/model_file"
                }
            ],
            "id": "#rescale_mrc.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Select classified particles by cinderella",
            "hints": [
                {
                    "dockerPull": "sbobkov/cinderella",
                    "class": "DockerRequirement"
                },
                {
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.input_class_dir)",
                        {
                            "entryname": "run.sh",
                            "entry": "sp_cinderella_predict.py -i $(inputs.input_class_dir.basename)/$(inputs.class_star_file.replace(\"_model.star\", \"_classes.mrcs\")) -w /src/model.h5 -o cinderella_tmp_output -t $(inputs.threshold) && /src/create_particles.py $(inputs.input_class_dir.basename)/$(inputs.class_star_file.replace(\"_model.star\", \"_data.star\")) -o $(inputs.outdir_name)/$(inputs.out_star_name) -c cinderella_tmp_output/*index_confidence.txt -t $(inputs.threshold) && cp cinderella_tmp_output/* $(inputs.outdir_name)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": "string",
                    "id": "#select_cinderella.cwl/class_star_file"
                },
                {
                    "type": "Directory",
                    "id": "#select_cinderella.cwl/input_class_dir"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "particles.star",
                    "id": "#select_cinderella.cwl/out_star_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "Select",
                    "id": "#select_cinderella.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0.5,
                    "id": "#select_cinderella.cwl/threshold"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#select_cinderella.cwl/outdir"
                },
                {
                    "type": "string",
                    "outputBinding": {
                        "outputEval": "$(inputs.out_star_name)"
                    },
                    "id": "#select_cinderella.cwl/star_file"
                }
            ],
            "id": "#select_cinderella.cwl"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "float",
                    "id": "#main/angpix"
                },
                {
                    "type": "float",
                    "id": "#main/autopick_LoG_adjust_threshold"
                },
                {
                    "type": "int",
                    "id": "#main/autopick_LoG_diam_max"
                },
                {
                    "type": "int",
                    "id": "#main/autopick_LoG_diam_min"
                },
                {
                    "type": "int",
                    "id": "#main/autopick_bg_radius"
                },
                {
                    "type": "int",
                    "id": "#main/autopick_extract_size"
                },
                {
                    "type": "int",
                    "id": "#main/autopick_lowpass"
                },
                {
                    "type": "int",
                    "id": "#main/autopick_scale"
                },
                {
                    "type": "int",
                    "id": "#main/class_2_num"
                },
                {
                    "type": "int",
                    "id": "#main/class_2_particle_diameter"
                },
                {
                    "type": "int",
                    "id": "#main/class_2_psi_step"
                },
                {
                    "type": "int",
                    "id": "#main/class_2_tau2_fudge"
                },
                {
                    "type": "int",
                    "id": "#main/class_num"
                },
                {
                    "type": "int",
                    "id": "#main/class_particle_diameter"
                },
                {
                    "type": "int",
                    "id": "#main/class_psi_step"
                },
                {
                    "type": "int",
                    "id": "#main/class_strict_highres_exp"
                },
                {
                    "type": "int",
                    "id": "#main/class_tau2_fudge"
                },
                {
                    "type": "float",
                    "id": "#main/ctf_amp_contrast"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_box"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_dAst"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_df_max"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_df_min"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_df_step"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_magnification"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_res_max"
                },
                {
                    "type": "int",
                    "id": "#main/ctf_res_min"
                },
                {
                    "type": "string",
                    "default": "micrographs_ctf.star",
                    "id": "#main/ctf_star_file"
                },
                {
                    "type": "float",
                    "id": "#main/dose_per_frame"
                },
                {
                    "type": "int",
                    "id": "#main/extract_2_bg_radius"
                },
                {
                    "type": "int",
                    "id": "#main/extract_2_scale"
                },
                {
                    "type": "int",
                    "id": "#main/extract_2_size"
                },
                {
                    "type": "int",
                    "id": "#main/extract_3_bg_radius"
                },
                {
                    "type": "int",
                    "id": "#main/extract_3_scale"
                },
                {
                    "type": "int",
                    "id": "#main/extract_3_size"
                },
                {
                    "type": "int",
                    "id": "#main/initial_model_healpix_order"
                },
                {
                    "type": "int",
                    "id": "#main/initial_model_rescale_new_box"
                },
                {
                    "type": "int",
                    "id": "#main/initial_model_threads"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#main/input_files"
                },
                {
                    "type": "int",
                    "id": "#main/model_particle_diameter"
                },
                {
                    "type": "string",
                    "id": "#main/model_symmetry"
                },
                {
                    "type": "string",
                    "default": "corrected_micrographs.star",
                    "id": "#main/motioncorr_starfile"
                },
                {
                    "type": "int",
                    "id": "#main/refine_model_healpix_order"
                },
                {
                    "type": "int",
                    "id": "#main/refine_model_ini_high"
                },
                {
                    "type": "int",
                    "id": "#main/refine_model_low_resol_join_halves"
                },
                {
                    "type": "int",
                    "id": "#main/refine_model_processes"
                },
                {
                    "type": "float",
                    "id": "#main/select_threshold"
                },
                {
                    "type": "float",
                    "id": "#main/sphere_aberration"
                },
                {
                    "type": "int",
                    "id": "#main/voltage"
                }
            ],
            "steps": [
                {
                    "run": "#class_2d.cwl",
                    "in": [
                        {
                            "source": "#main/class_num",
                            "id": "#main/class_2d/class_num"
                        },
                        {
                            "source": "#main/one_file_process/extract_star_file",
                            "valueFrom": "$(self[0])",
                            "id": "#main/class_2d/extract_star_file"
                        },
                        {
                            "source": "#main/combine_extract/outdir",
                            "id": "#main/class_2d/input_dir"
                        },
                        {
                            "source": "#main/class_particle_diameter",
                            "id": "#main/class_2d/particle_diameter"
                        },
                        {
                            "source": "#main/class_psi_step",
                            "id": "#main/class_2d/psi_step"
                        },
                        {
                            "source": "#main/class_strict_highres_exp",
                            "id": "#main/class_2d/strict_highres_exp"
                        },
                        {
                            "source": "#main/class_tau2_fudge",
                            "id": "#main/class_2d/tau2_fudge"
                        }
                    ],
                    "out": [
                        "#main/class_2d/outdir",
                        "#main/class_2d/star_file"
                    ],
                    "id": "#main/class_2d"
                },
                {
                    "run": "#class_2d.cwl",
                    "in": [
                        {
                            "source": "#main/class_2_num",
                            "id": "#main/class_2d_2/class_num"
                        },
                        {
                            "source": "#main/extract_2/star_file",
                            "id": "#main/class_2d_2/extract_star_file"
                        },
                        {
                            "source": "#main/extract_2/outdir",
                            "id": "#main/class_2d_2/input_dir"
                        },
                        {
                            "source": "#main/class_2_particle_diameter",
                            "id": "#main/class_2d_2/particle_diameter"
                        },
                        {
                            "source": "#main/class_2_psi_step",
                            "id": "#main/class_2d_2/psi_step"
                        },
                        {
                            "source": "#main/class_2_tau2_fudge",
                            "id": "#main/class_2d_2/tau2_fudge"
                        }
                    ],
                    "out": [
                        "#main/class_2d_2/outdir",
                        "#main/class_2d_2/star_file"
                    ],
                    "id": "#main/class_2d_2"
                },
                {
                    "run": "#combine_star_data.cwl",
                    "in": [
                        {
                            "source": "#main/one_file_process/ctf_outdir",
                            "id": "#main/combine_ctf/input_dirs"
                        },
                        {
                            "valueFrom": "CtfFind",
                            "id": "#main/combine_ctf/outdir_name"
                        }
                    ],
                    "out": [
                        "#main/combine_ctf/outdir"
                    ],
                    "id": "#main/combine_ctf"
                },
                {
                    "run": "#combine_star_data.cwl",
                    "in": [
                        {
                            "source": "#main/one_file_process/extract_outdir",
                            "id": "#main/combine_extract/input_dirs"
                        },
                        {
                            "valueFrom": "Extract",
                            "id": "#main/combine_extract/outdir_name"
                        }
                    ],
                    "out": [
                        "#main/combine_extract/outdir"
                    ],
                    "id": "#main/combine_extract"
                },
                {
                    "run": "#reextract.cwl",
                    "in": [
                        {
                            "source": "#main/angpix",
                            "id": "#main/extract_2/angpix"
                        },
                        {
                            "source": "#main/extract_2_bg_radius",
                            "id": "#main/extract_2/bg_radius"
                        },
                        {
                            "source": "#main/extract_2_size",
                            "id": "#main/extract_2/extract_size"
                        },
                        {
                            "source": "#main/combine_ctf/outdir",
                            "id": "#main/extract_2/input_micrographs_dir"
                        },
                        {
                            "source": "#main/select/outdir",
                            "id": "#main/extract_2/input_select_dir"
                        },
                        {
                            "source": "#main/ctf_star_file",
                            "id": "#main/extract_2/micrographs_star_file"
                        },
                        {
                            "valueFrom": "Extract_2",
                            "id": "#main/extract_2/outdir_name"
                        },
                        {
                            "source": "#main/extract_2_scale",
                            "id": "#main/extract_2/scale"
                        },
                        {
                            "source": "#main/select/star_file",
                            "id": "#main/extract_2/select_star_file"
                        }
                    ],
                    "out": [
                        "#main/extract_2/outdir",
                        "#main/extract_2/star_file"
                    ],
                    "id": "#main/extract_2"
                },
                {
                    "run": "#reextract.cwl",
                    "in": [
                        {
                            "source": "#main/angpix",
                            "id": "#main/extract_3/angpix"
                        },
                        {
                            "source": "#main/extract_3_bg_radius",
                            "id": "#main/extract_3/bg_radius"
                        },
                        {
                            "source": "#main/extract_3_size",
                            "id": "#main/extract_3/extract_size"
                        },
                        {
                            "source": "#main/combine_ctf/outdir",
                            "id": "#main/extract_3/input_micrographs_dir"
                        },
                        {
                            "source": "#main/select_2/outdir",
                            "id": "#main/extract_3/input_select_dir"
                        },
                        {
                            "source": "#main/ctf_star_file",
                            "id": "#main/extract_3/micrographs_star_file"
                        },
                        {
                            "valueFrom": "Extract_3",
                            "id": "#main/extract_3/outdir_name"
                        },
                        {
                            "source": "#main/extract_3_scale",
                            "id": "#main/extract_3/scale"
                        },
                        {
                            "source": "#main/select_2/star_file",
                            "id": "#main/extract_3/select_star_file"
                        }
                    ],
                    "out": [
                        "#main/extract_3/outdir",
                        "#main/extract_3/star_file"
                    ],
                    "id": "#main/extract_3"
                },
                {
                    "run": "#initial_model.cwl",
                    "in": [
                        {
                            "source": "#main/initial_model_healpix_order",
                            "id": "#main/initial_model/healpix_order"
                        },
                        {
                            "source": "#main/extract_2/outdir",
                            "id": "#main/initial_model/input_extract_dir"
                        },
                        {
                            "source": "#main/select_2/outdir",
                            "id": "#main/initial_model/input_select_dir"
                        },
                        {
                            "source": "#main/model_particle_diameter",
                            "id": "#main/initial_model/particle_diameter"
                        },
                        {
                            "source": "#main/select_2/star_file",
                            "id": "#main/initial_model/select_star_file"
                        },
                        {
                            "source": "#main/model_symmetry",
                            "id": "#main/initial_model/symmetry"
                        },
                        {
                            "source": "#main/initial_model_threads",
                            "id": "#main/initial_model/threads"
                        }
                    ],
                    "out": [
                        "#main/initial_model/model_file"
                    ],
                    "id": "#main/initial_model"
                },
                {
                    "scatter": "#main/one_file_process/input_file",
                    "in": [
                        {
                            "source": "#main/angpix",
                            "id": "#main/one_file_process/angpix"
                        },
                        {
                            "source": "#main/autopick_LoG_adjust_threshold",
                            "id": "#main/one_file_process/autopick_LoG_adjust_threshold"
                        },
                        {
                            "source": "#main/autopick_LoG_diam_max",
                            "id": "#main/one_file_process/autopick_LoG_diam_max"
                        },
                        {
                            "source": "#main/autopick_LoG_diam_min",
                            "id": "#main/one_file_process/autopick_LoG_diam_min"
                        },
                        {
                            "source": "#main/autopick_bg_radius",
                            "id": "#main/one_file_process/autopick_bg_radius"
                        },
                        {
                            "source": "#main/autopick_extract_size",
                            "id": "#main/one_file_process/autopick_extract_size"
                        },
                        {
                            "source": "#main/autopick_lowpass",
                            "id": "#main/one_file_process/autopick_lowpass"
                        },
                        {
                            "source": "#main/autopick_scale",
                            "id": "#main/one_file_process/autopick_scale"
                        },
                        {
                            "source": "#main/ctf_amp_contrast",
                            "id": "#main/one_file_process/ctf_amp_contrast"
                        },
                        {
                            "source": "#main/ctf_box",
                            "id": "#main/one_file_process/ctf_box"
                        },
                        {
                            "source": "#main/ctf_dAst",
                            "id": "#main/one_file_process/ctf_dAst"
                        },
                        {
                            "source": "#main/ctf_df_max",
                            "id": "#main/one_file_process/ctf_df_max"
                        },
                        {
                            "source": "#main/ctf_df_min",
                            "id": "#main/one_file_process/ctf_df_min"
                        },
                        {
                            "source": "#main/ctf_df_step",
                            "id": "#main/one_file_process/ctf_df_step"
                        },
                        {
                            "source": "#main/ctf_magnification",
                            "id": "#main/one_file_process/ctf_magnification"
                        },
                        {
                            "source": "#main/ctf_res_max",
                            "id": "#main/one_file_process/ctf_res_max"
                        },
                        {
                            "source": "#main/ctf_res_min",
                            "id": "#main/one_file_process/ctf_res_min"
                        },
                        {
                            "source": "#main/ctf_star_file",
                            "id": "#main/one_file_process/ctf_star_file"
                        },
                        {
                            "source": "#main/dose_per_frame",
                            "id": "#main/one_file_process/dose_per_frame"
                        },
                        {
                            "source": "#main/input_files",
                            "id": "#main/one_file_process/input_file"
                        },
                        {
                            "source": "#main/motioncorr_starfile",
                            "id": "#main/one_file_process/motioncorr_starfile"
                        },
                        {
                            "source": "#main/sphere_aberration",
                            "id": "#main/one_file_process/sphere_aberration"
                        },
                        {
                            "source": "#main/voltage",
                            "id": "#main/one_file_process/voltage"
                        }
                    ],
                    "run": {
                        "class": "Workflow",
                        "inputs": [
                            {
                                "type": "float",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/angpix"
                            },
                            {
                                "type": "float",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_LoG_adjust_threshold"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_LoG_diam_max"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_LoG_diam_min"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_bg_radius"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_extract_size"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_lowpass"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_scale"
                            },
                            {
                                "type": "float",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_amp_contrast"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_box"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_dAst"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_df_max"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_df_min"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_df_step"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_magnification"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_res_max"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_res_min"
                            },
                            {
                                "type": "string",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_star_file"
                            },
                            {
                                "type": "float",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/dose_per_frame"
                            },
                            {
                                "type": "File",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/input_file"
                            },
                            {
                                "type": "string",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr_starfile"
                            },
                            {
                                "type": "float",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/sphere_aberration"
                            },
                            {
                                "type": "int",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/voltage"
                            }
                        ],
                        "steps": [
                            {
                                "run": "#autopick.cwl",
                                "in": [
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_LoG_adjust_threshold",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/LoG_adjust_threshold"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_LoG_diam_max",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/LoG_diam_max"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_LoG_diam_min",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/LoG_diam_min"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/angpix",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/angpix"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/outdir",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/input_dir"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_lowpass",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/lowpass"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_star_file",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/star_file"
                                    }
                                ],
                                "out": [
                                    "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/outdir",
                                    "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/coord_suffix"
                                ],
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick"
                            },
                            {
                                "run": "#ctffind.cwl",
                                "in": [
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_amp_contrast",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/amp_contrast"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/angpix",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/angpix"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_box",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/box"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_dAst",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/dAst"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_df_max",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/df_max"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_df_min",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/df_min"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_df_step",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/df_step"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr/outdir",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/input_dir"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_magnification",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/magnification"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_res_max",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/res_max"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_res_min",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/res_min"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/sphere_aberration",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/sphere_aberration"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr_starfile",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/star_file"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/voltage",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/voltage"
                                    }
                                ],
                                "out": [
                                    "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/outdir"
                                ],
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind"
                            },
                            {
                                "run": "#extract_pick.cwl",
                                "in": [
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/angpix",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/angpix"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_bg_radius",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/bg_radius"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/coord_suffix",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/coord_suffix"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_extract_size",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/extract_size"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/outdir",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/input_micrographs_dir"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick/outdir",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/input_pick_dir"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_star_file",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/micrographs_star_file"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/autopick_scale",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/scale"
                                    }
                                ],
                                "out": [
                                    "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/outdir",
                                    "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/star_file"
                                ],
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick"
                            },
                            {
                                "run": "#motioncorr.cwl",
                                "in": [
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/angpix",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr/angpix"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/dose_per_frame",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr/dose_per_frame"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/input_file",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr/input_file"
                                    },
                                    {
                                        "source": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/voltage",
                                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr/voltage"
                                    }
                                ],
                                "out": [
                                    "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr/outdir"
                                ],
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/motioncorr"
                            }
                        ],
                        "outputs": [
                            {
                                "type": "Directory",
                                "outputSource": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctffind/outdir",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/ctf_outdir"
                            },
                            {
                                "type": "Directory",
                                "outputSource": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/outdir",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_outdir"
                            },
                            {
                                "type": "string",
                                "outputSource": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_pick/star_file",
                                "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422/extract_star_file"
                            }
                        ],
                        "id": "#main/one_file_process/1f62eab2-4384-428d-bd98-c2c96a7bd422"
                    },
                    "out": [
                        "#main/one_file_process/ctf_outdir",
                        "#main/one_file_process/extract_outdir",
                        "#main/one_file_process/extract_star_file"
                    ],
                    "id": "#main/one_file_process"
                },
                {
                    "run": "#refine_model.cwl",
                    "in": [
                        {
                            "source": "#main/refine_model_healpix_order",
                            "id": "#main/refine_model/healpix_order"
                        },
                        {
                            "source": "#main/refine_model_ini_high",
                            "id": "#main/refine_model/ini_high"
                        },
                        {
                            "source": "#main/extract_3/outdir",
                            "id": "#main/refine_model/input_dir"
                        },
                        {
                            "source": "#main/refine_model_low_resol_join_halves",
                            "id": "#main/refine_model/low_resol_join_halves"
                        },
                        {
                            "source": "#main/rescale_model/model_file",
                            "id": "#main/refine_model/model_file"
                        },
                        {
                            "source": "#main/model_particle_diameter",
                            "id": "#main/refine_model/particle_diameter"
                        },
                        {
                            "source": "#main/refine_model_processes",
                            "id": "#main/refine_model/processes"
                        },
                        {
                            "source": "#main/extract_3/star_file",
                            "id": "#main/refine_model/star_file"
                        },
                        {
                            "source": "#main/model_symmetry",
                            "id": "#main/refine_model/symmetry"
                        }
                    ],
                    "out": [
                        "#main/refine_model/outdir"
                    ],
                    "id": "#main/refine_model"
                },
                {
                    "run": "#rescale_mrc.cwl",
                    "in": [
                        {
                            "source": "#main/angpix",
                            "id": "#main/rescale_model/angpix"
                        },
                        {
                            "source": "#main/initial_model/model_file",
                            "id": "#main/rescale_model/input_mrc"
                        },
                        {
                            "source": "#main/initial_model_rescale_new_box",
                            "id": "#main/rescale_model/new_box"
                        },
                        {
                            "valueFrom": "initial_model_rescaled.mrc",
                            "id": "#main/rescale_model/outfile_name"
                        }
                    ],
                    "out": [
                        "#main/rescale_model/model_file"
                    ],
                    "id": "#main/rescale_model"
                },
                {
                    "run": "#select_cinderella.cwl",
                    "in": [
                        {
                            "source": "#main/class_2d/star_file",
                            "id": "#main/select/class_star_file"
                        },
                        {
                            "source": "#main/class_2d/outdir",
                            "id": "#main/select/input_class_dir"
                        },
                        {
                            "source": "#main/select_threshold",
                            "id": "#main/select/threshold"
                        }
                    ],
                    "out": [
                        "#main/select/outdir",
                        "#main/select/star_file"
                    ],
                    "id": "#main/select"
                },
                {
                    "run": "#select_cinderella.cwl",
                    "in": [
                        {
                            "source": "#main/class_2d_2/star_file",
                            "id": "#main/select_2/class_star_file"
                        },
                        {
                            "source": "#main/class_2d_2/outdir",
                            "id": "#main/select_2/input_class_dir"
                        },
                        {
                            "valueFrom": "Select_2",
                            "id": "#main/select_2/outdir_name"
                        },
                        {
                            "source": "#main/select_threshold",
                            "id": "#main/select_2/threshold"
                        }
                    ],
                    "out": [
                        "#main/select_2/outdir",
                        "#main/select_2/star_file"
                    ],
                    "id": "#main/select_2"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputSource": "#main/combine_ctf/outdir",
                    "id": "#main/combine_ctf_dir"
                },
                {
                    "type": "Directory",
                    "outputSource": "#main/combine_extract/outdir",
                    "id": "#main/combine_extract_dir"
                },
                {
                    "type": "Directory",
                    "outputSource": "#main/extract_2/outdir",
                    "id": "#main/extract_2_dir"
                },
                {
                    "type": "Directory",
                    "outputSource": "#main/extract_3/outdir",
                    "id": "#main/extract_3_dir"
                },
                {
                    "type": "Directory",
                    "outputSource": "#main/refine_model/outdir",
                    "id": "#main/refine_dir"
                },
                {
                    "type": "Directory",
                    "outputSource": "#main/select_2/outdir",
                    "id": "#main/select_2_dir"
                },
                {
                    "type": "Directory",
                    "outputSource": "#main/select/outdir",
                    "id": "#main/select_dir"
                }
            ],
            "id": "#main"
        }
    ],
    "cwlVersion": "v1.0"
}