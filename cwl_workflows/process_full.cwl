#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}

inputs: 
  input_files: File[]
  angpix: float
  voltage: int
  dose_per_frame: float
  sphere_aberration: float
  motioncorr_starfile:
    type: string
    default: "corrected_micrographs.star"
  ctf_amp_contrast: float
  ctf_magnification: int
  ctf_box: int
  ctf_res_min: int
  ctf_res_max: int
  ctf_df_min: int
  ctf_df_max: int
  ctf_df_step: int
  ctf_dAst: int
  ctf_star_file: 
    type: string
    default: "micrographs_ctf.star"
  autopick_LoG_diam_min: int
  autopick_LoG_diam_max: int
  autopick_LoG_adjust_threshold: float
  autopick_lowpass: int
  autopick_extract_size: int
  autopick_scale: int
  autopick_bg_radius: int
  class_tau2_fudge: int
  class_particle_diameter: int
  class_num: int
  class_strict_highres_exp: int
  class_psi_step: int
  select_threshold: float
  extract_2_size: int
  extract_2_scale: int
  extract_2_bg_radius: int 
  class_2_tau2_fudge: int
  class_2_particle_diameter: int
  class_2_num: int
  class_2_psi_step: int
  initial_model_healpix_order: int
  model_particle_diameter: int
  model_symmetry: string
  initial_model_threads: int
  initial_model_rescale_new_box: int
  extract_3_size: int
  extract_3_scale: int
  extract_3_bg_radius: int
  refine_model_healpix_order: int 
  refine_model_low_resol_join_halves: int
  refine_model_ini_high: int
  refine_model_processes: int

steps:
  one_file_process:
    scatter: input_file
    in: 
      input_file: input_files
      angpix: angpix
      voltage: voltage
      dose_per_frame: dose_per_frame
      sphere_aberration: sphere_aberration
      motioncorr_starfile: motioncorr_starfile
      ctf_amp_contrast: ctf_amp_contrast
      ctf_magnification: ctf_magnification
      ctf_box: ctf_box
      ctf_res_min: ctf_res_min
      ctf_res_max: ctf_res_max
      ctf_df_min: ctf_df_min
      ctf_df_max: ctf_df_max
      ctf_df_step: ctf_df_step
      ctf_dAst: ctf_dAst
      ctf_star_file: ctf_star_file
      autopick_LoG_diam_min: autopick_LoG_diam_min
      autopick_LoG_diam_max: autopick_LoG_diam_max
      autopick_LoG_adjust_threshold: autopick_LoG_adjust_threshold
      autopick_lowpass: autopick_lowpass
      autopick_extract_size: autopick_extract_size
      autopick_scale: autopick_scale
      autopick_bg_radius: autopick_bg_radius
    run: 
      class: Workflow
      inputs: 
        input_file: File
        angpix: float
        voltage: int
        dose_per_frame: float
        sphere_aberration: float
        motioncorr_starfile: string
        ctf_amp_contrast: float
        ctf_magnification: int
        ctf_box: int
        ctf_res_min: int
        ctf_res_max: int
        ctf_df_min: int
        ctf_df_max: int
        ctf_df_step: int
        ctf_dAst: int
        ctf_star_file: string
        autopick_LoG_diam_min: int
        autopick_LoG_diam_max: int
        autopick_LoG_adjust_threshold: float
        autopick_lowpass: int
        autopick_extract_size: int
        autopick_scale: int
        autopick_bg_radius: int
      steps:
        motioncorr:
          run: ../cwl_steps/motioncorr.cwl
          in:
            input_file: input_file
            angpix: angpix
            voltage: voltage
            dose_per_frame: dose_per_frame
          out: [outdir]
        ctffind:
          run: ../cwl_steps/ctffind.cwl
          in:
            input_dir: motioncorr/outdir
            star_file: motioncorr_starfile
            sphere_aberration: sphere_aberration
            angpix: angpix
            voltage: voltage
            amp_contrast: ctf_amp_contrast
            magnification: ctf_magnification
            box: ctf_box
            res_min: ctf_res_min
            res_max: ctf_res_max
            df_min: ctf_df_min
            df_max: ctf_df_max
            df_step: ctf_df_step
            dAst: ctf_dAst
          out: [outdir]
        autopick:
          run: ../cwl_steps/autopick.cwl
          in:
              input_dir: ctffind/outdir
              star_file: ctf_star_file
              angpix: angpix
              LoG_diam_min: autopick_LoG_diam_min
              LoG_diam_max: autopick_LoG_diam_max
              LoG_adjust_threshold: autopick_LoG_adjust_threshold
              lowpass: autopick_lowpass
          out: [outdir, coord_suffix]
        extract_pick:
          run: ../cwl_steps/extract_pick.cwl
          in:
              input_micrographs_dir: ctffind/outdir
              micrographs_star_file: ctf_star_file
              input_pick_dir: autopick/outdir
              coord_suffix: autopick/coord_suffix
              angpix: angpix
              extract_size: autopick_extract_size
              scale: autopick_scale
              bg_radius: autopick_bg_radius
          out: [outdir, star_file]
      outputs:
        ctf_outdir:
          type: Directory
          outputSource: ctffind/outdir
        extract_outdir:
          type: Directory
          outputSource: extract_pick/outdir
        extract_star_file:
          type: string
          outputSource: extract_pick/star_file
    out: [ctf_outdir, extract_outdir, extract_star_file]
  combine_ctf:
    run: ../cwl_steps/combine_star_data.cwl
    in: 
      input_dirs: one_file_process/ctf_outdir
      outdir_name:
        valueFrom: CtfFind
    out: [outdir]
  combine_extract:
    run: ../cwl_steps/combine_star_data.cwl
    in: 
      input_dirs: one_file_process/extract_outdir
      outdir_name:
        valueFrom: Extract
    out: [outdir]
  class_2d:
    run: ../cwl_steps/class_2d.cwl
    in:
      input_dir: combine_extract/outdir
      extract_star_file: 
        source: one_file_process/extract_star_file
        valueFrom: $(self[0])
      tau2_fudge: class_tau2_fudge
      particle_diameter: class_particle_diameter
      class_num: class_num
      strict_highres_exp: class_strict_highres_exp
      psi_step: class_psi_step
    out: [outdir, star_file]
  select:
    run: ../cwl_steps/select_cinderella.cwl
    in:
      input_class_dir: class_2d/outdir
      class_star_file: class_2d/star_file
      threshold: select_threshold
    out: [outdir, star_file]
  extract_2:
    run: ../cwl_steps/reextract.cwl
    in:
      input_micrographs_dir: combine_ctf/outdir
      micrographs_star_file: ctf_star_file
      input_select_dir: select/outdir
      select_star_file: select/star_file
      outdir_name:
        valueFrom: Extract_2
      angpix: angpix
      extract_size: extract_2_size
      scale: extract_2_scale
      bg_radius: extract_2_bg_radius
    out: [outdir, star_file]
  class_2d_2:
    run: ../cwl_steps/class_2d.cwl
    in:
      input_dir: extract_2/outdir
      extract_star_file: extract_2/star_file
      tau2_fudge: class_2_tau2_fudge
      particle_diameter: class_2_particle_diameter
      class_num: class_2_num
      psi_step: class_2_psi_step
    out: [outdir, star_file]
  select_2:
    run: ../cwl_steps/select_cinderella.cwl
    in:
      input_class_dir: class_2d_2/outdir
      class_star_file: class_2d_2/star_file
      threshold: select_threshold
      outdir_name:
        valueFrom: Select_2
    out: [outdir, star_file]
  initial_model:
    run: ../cwl_steps/initial_model.cwl
    in:
      input_extract_dir: extract_2/outdir
      input_select_dir: select_2/outdir
      select_star_file: select_2/star_file
      healpix_order: initial_model_healpix_order
      particle_diameter: model_particle_diameter
      symmetry: model_symmetry
      threads: initial_model_threads
    out: [model_file]
  rescale_model:
    run: ../cwl_steps/rescale_mrc.cwl
    in:
      input_mrc: initial_model/model_file
      outfile_name:
        valueFrom: initial_model_rescaled.mrc
      new_box: initial_model_rescale_new_box
      angpix: angpix
    out: [model_file]
  extract_3:
    run: ../cwl_steps/reextract.cwl
    in:
      input_micrographs_dir: combine_ctf/outdir
      micrographs_star_file: ctf_star_file
      input_select_dir: select_2/outdir
      select_star_file: select_2/star_file
      outdir_name:
        valueFrom: Extract_3
      angpix: angpix
      extract_size: extract_3_size
      scale: extract_3_scale
      bg_radius: extract_3_bg_radius
    out: [outdir, star_file]
  refine_model:
    run: ../cwl_steps/refine_model.cwl
    in: 
      input_dir: extract_3/outdir
      star_file: extract_3/star_file
      model_file: rescale_model/model_file
      healpix_order: refine_model_healpix_order
      particle_diameter: model_particle_diameter
      symmetry: model_symmetry
      low_resol_join_halves: refine_model_low_resol_join_halves
      ini_high: refine_model_ini_high
      processes: refine_model_processes
    out: [outdir]
outputs:
  combine_ctf_dir:
    type: Directory
    outputSource: combine_ctf/outdir
  combine_extract_dir:
    type: Directory
    outputSource: combine_extract/outdir
  select_dir:
    type: Directory
    outputSource: select/outdir
  extract_2_dir:
    type: Directory
    outputSource: extract_2/outdir
  select_2_dir:
    type: Directory
    outputSource: select_2/outdir
  extract_3_dir:
    type: Directory
    outputSource: extract_3/outdir
  refine_dir:
    type: Directory
    outputSource: refine_model/outdir
