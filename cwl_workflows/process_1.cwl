#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs: 
  input_files: File[]
  angpix: float
  voltage: int
  dose_per_frame: float
  sphere_aberration: float
  ctf_amp_contrast: float
  ctf_magnification: int
  ctf_box: int
  ctf_res_min: int
  ctf_res_max: int
  ctf_df_min: int
  ctf_df_max: int
  ctf_df_step: int
  ctf_dAst: int
  autopick_LoG_diam_min: int
  autopick_LoG_diam_max: int
  autopick_LoG_adjust_threshold: float
  autopick_lowpass: int
  autopick_extract_size: int
  autopick_scale: int
  autopick_bg_radius: int
  class_tau2_fudge: int
  class_particle_diameter: int
  class_num: int
  class_strict_highres_exp: int
  class_psi_step: int

steps:
  one_file_process:
    scatter: input_file
    in: 
      input_file: input_files
      angpix: angpix
      voltage: voltage
      dose_per_frame: dose_per_frame
      sphere_aberration: sphere_aberration
      ctf_amp_contrast: ctf_amp_contrast
      ctf_magnification: ctf_magnification
      ctf_box: ctf_box
      ctf_res_min: ctf_res_min
      ctf_res_max: ctf_res_max
      ctf_df_min: ctf_df_min
      ctf_df_max: ctf_df_max
      ctf_df_step: ctf_df_step
      ctf_dAst: ctf_dAst
      autopick_LoG_diam_min: autopick_LoG_diam_min
      autopick_LoG_diam_max: autopick_LoG_diam_max
      autopick_LoG_adjust_threshold: autopick_LoG_adjust_threshold
      autopick_lowpass: autopick_lowpass
      autopick_extract_size: autopick_extract_size
      autopick_scale: autopick_scale
      autopick_bg_radius: autopick_bg_radius
    run: 
      class: Workflow
      inputs: 
        input_file: File
        angpix: float
        voltage: int
        dose_per_frame: float
        sphere_aberration: float
        ctf_amp_contrast: float
        ctf_magnification: int
        ctf_box: int
        ctf_res_min: int
        ctf_res_max: int
        ctf_df_min: int
        ctf_df_max: int
        ctf_df_step: int
        ctf_dAst: int
        autopick_LoG_diam_min: int
        autopick_LoG_diam_max: int
        autopick_LoG_adjust_threshold: float
        autopick_lowpass: int
        autopick_extract_size: int
        autopick_scale: int
        autopick_bg_radius: int
      steps:
        motioncorr:
          run: ../cwl_steps/motioncorr.cwl
          in:
            input_file: input_file
            angpix: angpix
            voltage: voltage
            dose_per_frame: dose_per_frame
          out: [outdir, star_file]
        ctffind:
          run: ../cwl_steps/ctffind.cwl
          in:
            input_dir: motioncorr/outdir
            star_file: motioncorr/star_file
            sphere_aberration: sphere_aberration
            angpix: angpix
            voltage: voltage
            amp_contrast: ctf_amp_contrast
            magnification: ctf_magnification
            box: ctf_box
            res_min: ctf_res_min
            res_max: ctf_res_max
            df_min: ctf_df_min
            df_max: ctf_df_max
            df_step: ctf_df_step
            dAst: ctf_dAst
          out: [outdir, ctf_star_file]
        autopick:
          run: ../cwl_steps/autopick.cwl
          in:
              input_dir: ctffind/outdir
              star_file: ctffind/ctf_star_file
              angpix: angpix
              LoG_diam_min: autopick_LoG_diam_min
              LoG_diam_max: autopick_LoG_diam_max
              LoG_adjust_threshold: autopick_LoG_adjust_threshold
              lowpass: autopick_lowpass
          out: [outdir, coord_suffix]
        extract_pick:
          run: ../cwl_steps/extract_pick.cwl
          in:
              input_micrographs_dir: ctffind/outdir
              micrographs_star_file: ctffind/ctf_star_file
              input_pick_dir: autopick/outdir
              coord_suffix: autopick/coord_suffix
              angpix: angpix
              extract_size: autopick_extract_size
              scale: autopick_scale
              bg_radius: autopick_bg_radius
          out: [outdir, star_file]
      outputs:
        ctf_outdir:
          type: Directory
          outputSource: ctffind/outdir
        extract_outdir:
          type: Directory
          outputSource: extract_pick/outdir
        ctf_star_file:
          type: string
          outputSource: ctffind/ctf_star_file
        extract_star_file:
          type: string
          outputSource: extract_pick/star_file
    out: [ctf_outdir, extract_outdir, ctf_star_file, extract_star_file]
  combine_ctf:
    run: ../cwl_steps/combine_star_data.cwl
    in: 
      input_dirs: one_file_process/ctf_outdir
      outdir_name:
        valueFrom: CtfFind
    out: [outdir]
  combine_extract:
    run: ../cwl_steps/combine_star_data.cwl
    in: 
      input_dirs: one_file_process/extract_outdir
      outdir_name:
        valueFrom: Extract
    out: [outdir]
  class_2d:
    run: ../cwl_steps/class_2d.cwl
    in:
      input_dir: combine_extract/outdir
      extract_star_file: 
        source: one_file_process/extract_star_file
        valueFrom: $(self[0])
      tau2_fudge: class_tau2_fudge
      particle_diameter: class_particle_diameter
      class_num: class_num
      strict_highres_exp: class_strict_highres_exp
      psi_step: class_psi_step
    out: [outdir, star_file]
outputs:
  outclass:
    type: Directory
    outputSource: class_2d/outdir
  ctf_outdir:
    type: Directory
    outputSource: combine_ctf/outdir
  extract_outdir:
    type: Directory
    outputSource: combine_extract/outdir
