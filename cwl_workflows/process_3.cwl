#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}

inputs: 
  input_extract_dir: Directory
  input_select_dir: Directory
  select_star_file: string
  input_micrographs_dir: Directory
  micrographs_star_file: string
  initial_healpix_order: int
  particle_diameter: int
  symmetry: string
  initial_model_threads: int
  rescale_new_box: int
  angpix: float
  reextract_size: int
  reextract_scale: int
  reextract_bg_radius: int
  refine_healpix_order: int 
  refine_low_resol_join_halves: int
  refine_ini_high: int
  refine_processes: int

steps:
  initial_model:
    run: ../cwl_steps/initial_model.cwl
    in:
      input_extract_dir: input_extract_dir
      input_select_dir: input_select_dir
      select_star_file: select_star_file
      healpix_order: initial_healpix_order
      particle_diameter: particle_diameter
      symmetry: symmetry
      threads: initial_model_threads
    out: [model_file]
  rescale_model:
    run: ../cwl_steps/rescale_mrc.cwl
    in:
      input_mrc: initial_model/model_file
      outfile_name:
        valueFrom: initial_model_rescaled.mrc
      new_box: rescale_new_box
      angpix: angpix
    out: [model_file]
  reextract:
    run: ../cwl_steps/reextract.cwl
    in:
      input_micrographs_dir: input_micrographs_dir
      micrographs_star_file: micrographs_star_file
      input_select_dir: input_select_dir
      select_star_file: select_star_file
      angpix: angpix
      extract_size: reextract_size
      scale: reextract_scale
      bg_radius: reextract_bg_radius
    out: [outdir, star_file]
  refine_model:
    run: ../cwl_steps/refine_model.cwl
    in: 
      input_dir: reextract/outdir
      star_file: reextract/star_file
      model_file: rescale_model/model_file
      healpix_order: refine_healpix_order
      particle_diameter: particle_diameter
      symmetry: symmetry
      low_resol_join_halves: refine_low_resol_join_halves
      ini_high: refine_ini_high
      processes: refine_processes
    out: [outdir]
outputs:
  refine_dir:
    type: Directory
    outputSource: refine_model/outdir
