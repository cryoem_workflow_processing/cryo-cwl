#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

inputs: 
  input_micrographs_dir: Directory
  micrographs_star_file: string
  input_select_dir: Directory
  select_star_file: string
  angpix: float
  reextract_size: int
  reextract_scale: int
  reextract_bg_radius: int 
  class_tau2_fudge: int
  class_particle_diameter: int
  class_num: int
  class_psi_step: int

steps:
  reextract:
    run: ../cwl_steps/reextract.cwl
    in:
        input_micrographs_dir: input_micrographs_dir
        micrographs_star_file: micrographs_star_file
        input_select_dir: input_select_dir
        select_star_file: select_star_file
        angpix: angpix
        extract_size: reextract_size
        scale: reextract_scale
        bg_radius: reextract_bg_radius
    out: [outdir, star_file]
  class_2d:
    run: ../cwl_steps/class_2d.cwl
    in:
      input_dir: reextract/outdir
      extract_star_file: reextract/star_file
      tau2_fudge: class_tau2_fudge
      particle_diameter: class_particle_diameter
      class_num: class_num
      psi_step: class_psi_step
    out: [outdir, star_file]
outputs:
  reextractdir:
    type: Directory
    outputSource: reextract/outdir
  outclass:
    type: Directory
    outputSource: class_2d/outdir
  star_file:
    type: string
    outputSource: class_2d/star_file
