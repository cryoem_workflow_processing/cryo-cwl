#/bin/bash

# Set output folder path
processed=/mnt/data/exp/cryoem/TvNiR/processed/cwl_process

i=6
cwl-runner  --outdir $processed/out_tmp/$i/ \
            --tmpdir-prefix $processed/tmp/$i/ \
            --cachedir $processed/cache/$i/ \
            --parallel --timestamps 1>$processed/out_tmp.$i.log 2>&1 \
            --target select_dir \
            cwl_workflows/process_full.cwl jobdata/process_full_job_tmp.yml