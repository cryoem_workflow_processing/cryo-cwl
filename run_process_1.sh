#/bin/bash
source /mnt/data/sw/anaconda3/bin/activate

# Set output folder path
processed=/mnt/data/exp/cryoem/TvNiR/processed/cwl_process

i=1
cwl-runner  --outdir $processed/out/1_$i/ \
            --tmpdir-prefix $processed/tmp/1_$i/ \
            --cachedir $processed/cache/1_$i/ \
            --parallel --timestamps 1>$processed/out/1_$i.log 2>&1 \
            cwl_workflows/process_1.cwl jobdata/process_1_job.yml