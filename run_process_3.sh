#/bin/bash
source /mnt/data/sw/anaconda3/bin/activate

# Set output folder path
processed=/mnt/data/exp/cryoem/TvNiR/processed/cwl_process

i=1
cwl-runner  --outdir $processed/out/3_$i/ \
            --tmpdir-prefix $processed/tmp/3_$i/ \
            --cachedir $processed/cache/3_$i/ \
            --parallel --timestamps 1>$processed/out/3_$i.log 2>&1 \
            cwl_workflows/process_3.cwl jobdata/process_3_job.yml